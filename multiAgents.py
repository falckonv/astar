# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation     function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {NORTH, SOUTH, WEST, EAST, STOP}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"
        return successorGameState.getScore()

def scoreEvaluationFunction(currentGameState):
    """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
    """
    return currentGameState.getScore()


"""
MINIMAX-SEARCH()

searchMiniMax() is started by pacmans move. Pacmans optimal moves are calculated 
according to the chosen depth. One depth step correspons to one move by pacman.

Two helper functions are used

maxValue() and minValue()

Input:

    gameState ......... the current gameState as a GameState object
    
Returns:

    move .............. the best move for pacman according to the depth restricted backtracking


"""
def searchMiniMax(gameState, depth):
    _, preferedMove = maxValue(gameState, depth)
    return preferedMove
    
    
def maxValue(gameState, depth):  # Need game + state, but both are contained in gameState
    
    preferedMove = 'Stop'   # Stop
    
    if (gameState.isWin() or gameState.isLose() or not depth):  # Or max-depth reached
        score = scoreEvaluationFunction(gameState)
        return score, preferedMove

    value = -999
    depth = depth - 1
    
    # Get all pacmans legal moves from his current position
    legalMoves = gameState.getLegalActions(0)
    for move in legalMoves:
        newGameState  = gameState.generateSuccessor(0, move)
        value2, move2 = minValue(newGameState, depth)  # Returns a list of values and moves (dont need ghosts moves)
   
        if (value2  > value): 
            value        = value2  # value2 = sum of pacman-score for this current pacman move
            preferedMove = move   # pacmans currently best move (choosing the ghost batch with highest pacman scores)
  
    return value, preferedMove


def minValue(gameState, depth):
    preferedMove = 'Stop'
    
    if (gameState.isWin() or gameState.isLose()):  # Or max-depth reached (not on ghosts)
        score = scoreEvaluationFunction(gameState)
        return score, preferedMove
    
    batchValue = 999  # Must keep a value for the total sum of ghosts
    container  = 0    # Keep count
    
    ghostNumber = gameState.getNumAgents() - 1
    ghostValues = []
    ghostMoves  = []
    keepDepth   = depth
    
    # MUST UPDATE THIS TO TAKE A LIST OF GHOSTS and run this multiple times
    # WRAP in another for-loop with all ghosts
    for x in range(ghostNumber):
        x = x + 1   # Ghosts agentIndex is from 1 to getNumAgents()
        value = 999 # Must be reset before each ghosts run
        ghostsActions = gameState.getLegalActions(x)
        
        # loop through a single ghosts legal moves (chose the lowest and add to list)
        for action in ghostsActions:
            
            newGamestate = gameState.generateSuccessor(x, action)
            value2, move2 = maxValue(newGamestate, depth) # The gameState resulting after "action"
            if value2 < value:
                value = value2
                preferedMove = move2  # We do probably not need to track ghosts next moves
                
        depth = keepDepth
        
    # Add the lowest values and corresponding moves for the ghost            
    ghostValues.append(value)
    ghostMoves.append(preferedMove)
    
            
    # DELAY RETURN TO THE batch of ghosts giving the HIGEST pacman scores
                                # value is the sum of values for all ghosts (or an array with values)
    return (sum(ghostValues)), ghostMoves  # preferedMove have to be an array of all ghosts move (if necessary)
            
   

    
    
"""
Alpha-beta-pruning

Alpha-beta pruning is based on the code from searchMiniMax() 

Two parameters are entered: alpha and beta which comprise the upper and lower bound constraints.
If values are outside the ranges, they will never be explored and can therefore be cut off.

"""
def searchAlphaBeta(gameState, depth):
    _, preferedMove = maxValueAB(gameState, depth, alpha=-999, beta=999)
    return preferedMove
    
    
def maxValueAB(gameState, depth, alpha, beta):  
    preferedMove = 'Stop' 
    
    if (gameState.isWin() or gameState.isLose() or not depth):  # Or max-depth reached
        score = scoreEvaluationFunction(gameState)
        return score, preferedMove

    value = -999
    depth = depth - 1
    
    # Get all pacmans legal moves from his current position
    legalMoves = gameState.getLegalActions(0)
    for move in legalMoves:
        newGameState  = gameState.generateSuccessor(0, move)
        value2, move2 = minValueAB(newGameState, depth, alpha, beta)  # Returns a list of values and moves (dont need ghosts moves)
   
        if (value2  > value): 
            value        = value2  # value2 = sum of pacman-score for this current pacman move
            preferedMove = move   # pacmans currently best move (choosing the ghost batch with highest pacman scores)
            
            # Update alpha
            alpha = value
            
        if (value >= beta):
            return value, preferedMove
  
    return value, preferedMove


def minValueAB(gameState, depth, alpha, beta):
    preferedMove = 'Stop'
    
    if (gameState.isWin() or gameState.isLose()):  # Or max-depth reached (not on ghosts)
        score = scoreEvaluationFunction(gameState)
        return score, preferedMove
    
    batchValue = 999  # Must keep a value for the total sum of ghosts
    container  = 0    # Keep count
    
    ghostNumber = gameState.getNumAgents() - 1
    ghostValues = []
    ghostMoves  = []
    
    # MUST UPDATE THIS TO TAKE A LIST OF GHOSTS and run this multiple times
    # WRAP in another for-loop with all ghosts
    for x in range(ghostNumber):
        x = x + 1   # Ghosts agentIndex is from 1 to getNumAgents()
        value = 999 # Must be reset before each ghosts run
        ghostsActions = gameState.getLegalActions(x)
        
        # loop through a single ghosts legal moves (chose the lowest and add to list)
        for action in ghostsActions:
            
            newGamestate = gameState.generateSuccessor(x, action)
            value2, move2 = maxValueAB(newGamestate, depth, alpha, beta) # The gameState resulting after "action"
            if value2 < value:
                value = value2
                preferedMove = move2  # We do probably not need to track ghosts next moves
                
                # Update beta
                beta = value 
                
        
    # Add the lowest values and corresponding moves for the ghost            
    ghostValues.append(value)
    ghostMoves.append(preferedMove)
    
    if ((sum(ghostValues)) <= alpha):
        return (sum(ghostValues)), ghostMoves
            
    # DELAY RETURN TO THE batch of ghosts giving the HIGEST pacman scores
                                # value is the sum of values for all ghosts (or an array with values)
    return (sum(ghostValues)), ghostMoves  # preferedMove have to be an array of all ghosts move (if necessary)    
    

class MultiAgentSearchAgent(Agent):
    """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
    Your minimax agent (question 2)
    """
            

    def getAction(self, gameState):
        """
        Returns the minimax action from the current gameState using self.depth
        and self.evaluationFunction.

        Here are some method calls that might be useful when implementing minimax.

        gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

        gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

        gameState.getNumAgents():
        Returns the total number of agents in the game

        gameState.isWin():
        Returns whether or not the game state is a winning state

        gameState.isLose():
        Returns whether or not the game state is a losing state
        """
        "*** YOUR CODE HERE ***"
        #util.raiseNotDefined()
        
        
        # OWN CODE ADDED BELOW
   
        depth = self.depth
        return searchMiniMax(gameState, depth)
            
        
        

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
    Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
        Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        #util.raiseNotDefined()
        
        depth = self.depth
        return searchAlphaBeta(gameState, depth)
        
        
        
        
        

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
        Returns the expectimax action using self.depth and self.evaluationFunction

        All ghosts should be modeled as choosing uniformly at random from their
        legal moves.
        """
        "*** YOUR CODE HERE ***"
        util.raiseNotDefined()

def betterEvaluationFunction(currentGameState):
    """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()

# Abbreviation
better = betterEvaluationFunction
